import click
from flask.cli import with_appcontext

from . import db
from .models import User


@click.command("db-init")
@with_appcontext
def db_init_command():
    """Clear the existing data and create new tables."""
    db.init_db()
    click.echo("Database initialized.")


@click.command("user-create")
@click.argument("username")
@click.password_option()
@with_appcontext
def user_create_command(username, password):
    """Create a user in database."""
    user = User.create(username=username, password=password)
    click.echo("User {} initialized.".format(user.username))


def init_app(app):
    """Register CLI commands to the application."""
    app.cli.add_command(db_init_command)
    app.cli.add_command(user_create_command)
