(function () {
    var folders = document.querySelectorAll('.folder');
    folders.forEach(function (folder) {
        var folderToggle = folder.querySelector('.folder-toggle');
        folderToggle.onclick = function () {
            folder.classList.toggle('folder-folded');
        };
    });

    var closableButtons = document.querySelectorAll('button[data-close]');
    closableButtons.forEach(function (button) {
        button.addEventListener('click', function () {
            var idToClose = button.getAttribute('data-close');
            var elementToClose = document.getElementById(idToClose);
            if (elementToClose) {
                elementToClose.remove();
            }
        });
    });

    var buttonsToConfirm = document.querySelectorAll('button[data-confirm]');
    buttonsToConfirm.forEach(function (button) {
        button.addEventListener('click', function (e) {
            var message = e.target.getAttribute('data-confirm');
            if (!window.confirm(message)) {
                e.preventDefault();
            }
        });
    });

    var buttonsToAction = document.querySelectorAll('button[data-action]');
    buttonsToAction.forEach(function (button) {
        button.addEventListener('click', function (e) {
            if (e.defaultPrevented) {
                return false;
            }

            var action = e.target.getAttribute('data-action');
            var method = e.target.getAttribute('data-method') || 'get';
            window.fetch(action, { method: method.toUpperCase() })
                .then(function (response) {
                    if (response.redirected) {
                        window.location = response.url;
                    }
                })
                .catch(function (error) {
                    console.error(error, error.stack);
                    window.alert('Une erreur sauvage est apparue ! (et on sait pas encore bien la gérer) Si tu sais faire, peux-tu ouvrir la console de ton navigateur et envoyer aux développeur·euses ce qui apparaît dedans ?');
                });
        });
    });
}());
