# Reflets

Reflets était un mini-projet destiné à rendre compte des temps forts de
l’association [Framasoft](https://framasoft.org). Nous utilisions jusque là un
wiki interne pour lister les évènements rythmant la vie de l’association, mais
sa difficulté à être édité et son manque d’esthétisme nous a poussé à vouloir
essayer quelque chose de différent. Ce projet devait aussi permettre de donner
une meilleure visibilité aux projets aussi bien internes qu’externes.

**Reflets est désormais terminé et non maintenu.** En effet, l’expérience ne
s’est pas révélée concluante pour différentes raisons :

- c’était un nouvel outil (un de plus) ;
- il faisait plus ou moins doublon avec le wiki ;
- il n’était pas dans nos pattes (donc pas indispensable) pour qu’on soit
  obligé de penser à lui.

Le projet n’évoluera donc plus à l’avenir (pas même pour corriger d’éventuels
bugs).

**Vous pouvez toutefois considérer Reflets comme étant fonctionnel.**
C’est-à-dire que vous pouvez :

- indiquer des évènements (marquants) de votre structure ;
- leur associer une description et un projet (pour regrouper certains évènements) ;
- rendre les évènements ou les projets publics ou privés selon la
  confidentialité de chacun, ainsi que de votre volonté à communiquer vers
  l’extérieur.

**Reflets est distribué sous licence [AGPL 3](./LICENSE).** N’hésitez pas à
forker et repartir de ce projet pour vos propres besoins.

La documentation technique se trouve dans [le répertoire `./docs`](./docs/index.md).
