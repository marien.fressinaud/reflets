# Documentation technique

## Stack technique

Reflets est écrit en Python 3.6+ pour le backend et repose sur le
microframework [Flask](http://flask.pocoo.org/). Les données sont stockées dans
une base SQLite, et nous n'utilisons pas d'ORM. Ce choix pourra évoluer par la
suite.

Le frontend est écrit en pur HTML et CSS (i.e. sans framework type Bootstrap).
Pour ce qui est du JavaScript, on se contentera de commencer sans framework non
plus mais [VueJS](https://vuejs.org/) est envisagé à terme si le besoin se fait
sentir.

Le maître mot de cette stack est : simplicité. Le but est d'avoir du code
facile à prendre en main, en particulier pour les personnes apprenant à
développer. La documentation est donc importante à maintenir en parallèle.

## Installation des dépendances

Cette documentation assume que vous êtes sous Linux (les instructions
pourraient aussi fonctionner sous MacOS mais je ne peux pas le garantir). Git
doit également être installé pour récupérer et travailler sur le projet ([plus
d'infos ici](https://git-scm.com/downloads)).

Les commandes listées ci-dessous doivent être exécutées dans une console (aussi
appelée terminal). Elles commencent toutes par un `$` : cela correspond au
"prompt" de la console et ne doit pas être tapé. Un chemin peut être indiqué
avant le `$` (exemple : `reflets$`), cela afin de vous aider à vous situer dans
l'arborescence des répertoires.

Assurez-vous d'avoir Python 3.6+ installé sur votre ordinateur. Dans une
console, exécutez :

```console
$ python3 -V
Python 3.7.3
```

Si le numéro de version qui s'affiche n'est pas le même mais est supérieur ou
égal à 3.6, c'est bon aussi ! En cas d'erreur ou de problème vous pouvez jeter
un coup d'œil [au site officiel de Python](https://www.python.org/downloads/).
Si vous êtes sous Linux, préférez utiliser votre gestionnaire de paquets favori.

Téléchargez maintenant le projet Git avec la commande :

```console
$ git clone git@framagit.org:marien.fressinaud/reflets.git
$ # OU
$ git clone https://framagit.org/marien.fressinaud/reflets.git
$ cd reflets
```

Créez-vous maintenant un environnement Python qui sera propre à votre projet :

```console
reflets$ python3 -m venv env
```

Un répertoire `env` devrait être maintenant apparu dans votre projet. Activez
l'environnement avec :

```console
reflets$ source env/bin/activate
```

Une fois cet environnement activé, toutes les dépendances installées seront
confinées dedans. Cela évite notamment que plusieurs programmes écrits en
Python ne se marchent sur les pieds sur votre PC. Vous pourrez à tout moment
désactiver l'environnement avec (ne le faites toutefois pas immédiatement) :

```console
reflets$ deactivate
```

Reflets permet de se connecter en LDAP et nécessite pour cela du module
[python-ldap](https://www.python-ldap.org). Ce module nécessite d’installer des
dépendances supplémentaires depuis votre gestionnaire de paquets. Vous pourrez
trouver la commande à exécuter [dans leur documentation
officielle](https://www.python-ldap.org/en/python-ldap-3.2.0/installing.html#build-prerequisites).

Pour installer maintenant les dépendances du projet, exécutez :

```console
reflets$ pip install -r requirements.txt
```

Si vous disposez de la commande `make` sur votre PC, vous pouvez aussi utiliser :

```console
reflets$ make dep
```

Vous risquez d'avoir besoin des fichiers de développement de OpenLDAP :

```console
$ # Sur Ubuntu and co
$ sudo apt-get install libsasl2-dev python-dev libldap2-dev libssl-dev
$ # Sur Fedora and co
$ sudo dnf install python-devel openldap-devel
```

## Initialiser la base de données

Avant de lancer l'application, il est nécessaire d'initialiser la base de
données avec :

```console
reflets$ FLASK_APP=src FLASK_ENV=development flask db-init
reflets$ FLASK_APP=src FLASK_ENV=development flask user-create admin --password secret
reflets$ # ou avec la commande make
reflets$ make db-setup
```

Cela va créer les tables en base de données et créer un utilisateur avec les
identifiants `admin` / `secret`.

## Lancer l'application (développement)

Une fois les dépendances installées, pour lancer l'application, exécutez la
commande suivante :

```console
reflets$ FLASK_APP=src FLASK_ENV=development flask run
reflets$ # ou avec la commande make
reflets$ make run
```

Cela exécute Flask (le framework utilisé pour le backend) en lui précisant que
l'application se trouve dans le répertoire `src` et de se lancer en mode
`development` pour qu'il détecte automatiquement les changements faits dans le
code.

Vous pouvez désormais vous connecter en allant sur [127.0.0.1:5000](http://127.0.0.1:5000/)
et en saisissant les identifiants donnés plus haut.

## Déployer en production (Nginx + uWSGI)

Nous supposons dans cette partie que vous disposez :

- d'un serveur sous Linux accessible à distance
- d'un nom de domaine pointant sur ce serveur (ex. mon-domaine.net)

Dans notre cas, nous voulons que Reflets soit accessible à l'adresse
`http://mon-domaine.net/reflets`.

Commencez par installer Nginx et uWSGI depuis votre gestionnaire de paquets.

Nous supposons également que vous avez installé les dépendances comme précisé
plus haut. Vous devrez également avoir initialisé la base de données, mais
faites attention à préciser le bon environnement (i.e. `production`) et à
utiliser un nom d'utilisateur et un mot de passe plus sécurisés. vous devrez
donc taper les commandes à la main sans passer par le raccourci `make` :

```console
reflets$ FLASK_APP=src FLASK_ENV=production flask db-init
reflets$ FLASK_APP=src FLASK_ENV=production flask user-create NOM_UTILISATEUR
reflets$ # Un mot de passe devrait vous être demandé
```

Si vous souhaitez vous connecter par LDAP, vous n'avez pas besoin de créer
d'utilisateur. Vous pouvez préciser la variable d'environnement
`REFLETS_DATABASE` indiquant un chemin particulier pour votre base de données.

Créez ensuite un fichier de configuration Nginx (ex. `/etc/nginx/sites-available/reflets.conf`) :

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name mon-domaine.net;

    location /reflets {
        try_files $uri @reflets;
    }

    location @reflets {
        include uwsgi_params;
        uwsgi_pass unix:/tmp/reflets.sock;
    }
}
```

Cette configuration dit à Nginx de transférer toutes les requêtes arrivant à
`http://mon-domaine.net/reflets` sur une socket Unix, `/tmp/reflets.sock`. Si
vous souhaitez servir Reflets à la racine (i.e. `http://mon-domaine.net/`),
remplacez simplement `location /reflets` par `location /`.

Activez ensuite le fichier de configuration avec la commande suivante :

```console
$ ln -s /etc/nginx/sites-available/reflets.conf /etc/nginx/sites-enabled/reflets.conf
```

Celle-ci crée un lien symbolique vers votre fichier dans le répertoire
`/etc/nginx/sites-enabled`.

Il faut maintenant configurer uWSGI qui va mettre la socket en place. Créez un
fichier de configuration (ex. `/etc/uwsgi/apps-available/reflets.ini`) :

```ini
[uwsgi]
env = FLASK_ENV=production
env = REFLETS_DATABASE=/path/to/reflets/instance/db.sqlite
env = REFLETS_SECRET_KEY=a-random-string

uid = www-data
gid = www-data
chdir = /path/to/reflets/
home  = /path/to/reflets/env/
mount = /reflets=src:create_app()
manage-script-name = true

master          = true
processes       = 2
socket          = /tmp/reflets.sock
```

Vous devez adapter ce fichier, en particulier les variables d'environnement :

- `REFLETS_DATABASE` indique le chemin vers la base de données SQLite ; elle
  peut se situer n'importe où sur le serveur tant que l'utilisateur `www-data`
  peut écrire dedans
- `REFLETS_SECRET_KEY` est une chaîne aléatoire, générée par exemple avec
  OpenSSL (`openssl rand -base64 42`)

Vous devrez également adapter les chemins de `chdir` et de `home` (c'est cette
dernière qui va permettre de trouver les dépendances installées dans
l'environnement Python). Si vous souhaitez servir Reflets à la racine du nom de
domaine, remplacez la directive `mount` par `mount = /=src:create_app()`.

Vous noterez enfin le chemin vers la socket qui doit évidemment être le même
que celui définit dans la configuration Nginx.

Bien sûr, sentez-vous libre d'adapter cette configuration à votre situation et
de compléter les fichiers de configuration avec les options pertinentes (nous
ne souhaitons pas être exhaustifs pour des raisons de clareté).

Activez ensuite le fichier de configuration avec la commande suivante (même
principe que pour Nginx) :

```console
$ ln -s /etc/uwsgi/apps-available/reflets.ini /etc/uwsgi/apps-enabled/reflets.ini
```

Une fois ces deux fichiers activés, il faut redémarrer les services. Si vous
utilisez systemd, vous devrez lancer en `root` :

```console
# # Vérifiez d'abord que vous n'avez pas fait d'erreur dans la configuration Nginx
# nginx -t
# systemctl reload nginx
# systemctl restart uwsgi
```

Si tout est bien configuré, vous devriez maintenant pouvoir vous connecter à
l'adresse `http://mon-domaine.net/reflets`. Il ne vous reste plus qu'à
récupérer un certificat HTTPS avec [Let's Encrypt](https://letsencrypt.org/)
par exemple.

Vous aurez peut-être des problèmes d'accès aux fichiers, vérifiez bien que
l'utilisateur `www-data` a accès au répertoire contenant le code de Reflets :

```console
# chown -R www-data:www-data /path/to/reflets
# # Et si vous avez déplacé votre base de données
# chown www-data:www-data /path/to/db.sqlite
```

## Configurer un serveur LDAP (facultatif)

Pour faire fonctionner LDAP, vous devrez définir 4 variables d'environnement
supplémentaires lors du déploiement de Reflets (dans le fichier de
configuration de uWSGI par exemple) :

- `REFLETS_LDAP_AUTH_SERVER` qui définit l'url vers le serveur LDAP (commençant
  par `ldap://` ou, de préférence, `ldaps://`)
- `REFLETS_LDAP_DN` correspond au DN (_Distinguished Name_) dans lequel
  chercher les utilisateurs (ex. `ou=users,dc=mon-domaine,dc=net`)
- `REFLETS_LDAP_USERNAME` est le nom d'utilisateur avec lequel se connecter au
  LDAP (ex. `uid=ldap,ou=users,dc=mon-domaine,dc=net`)
- `REFLETS_LDAP_PASSWORD` correspond évidemment au mot de passe de
  l'utilisateur

Une fois ces variables définies, Reflets va automatiquement détecter que vous
souhaitez vous connecter avec LDAP. Vous pouvez vérifier qu'il est configuré en
vous rendant sur la page de connexion qui doit désormais afficher `Connexion
(LDAP)`, puis essayez de vous connecter.

## Mettre Reflets à jour

Pour mettre à jour Reflets, il y a peu de choses à faire. Cela consiste
seulement (pour le moment) à récupérer la dernière version du code avec Git :

```console
reflets$ git checkout master
reflets$ # Vérifiez que vous n'avez pas fait de modifications
reflets$ git status
reflets$ # Si tout est bon
reflets$ git pull
```

N'oubliez pas de redémarrer le service `uwsgi` pour qu'il prenne en compte le
nouveau code :

```console
# systemctl restart uwsgi
```

Si des vérifications supplémentaires exceptionnelles sont à faire, elles seront
listées dans un fichier `CHANGELOG` (qui n'existe pas encore).

## Ajouter des dépendances

Avant d'ajouter une dépendance au projet, merci de réfléchir à son utilité. Si
le choix est justifié, voici la procédure à suivre.

Vérifiez d'abord que vous avez bien activé l'environnement Python avec :

```console
reflets$ source env/bin/activate
```

Installez ensuite la dépendance avec `pip` :

```console
reflets$ pip install [votre-dependance]
```

Puis pensez à mettre à jour le fichier `requirements.txt` avec :

```console
reflets$ pip freeze > requirements.txt
```

Ajoutez aussi votre dépendance au fichier [`setup.cfg`](../setup.cfg), avec les
autres `install_requires`.
