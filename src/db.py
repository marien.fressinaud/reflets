import sqlite3

from flask import current_app, g


def get_db():
    """Return the app database."""
    if "db" not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"], detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def init_db():
    """Initialize the database with the src/schema.sql file."""
    db = get_db()

    with current_app.open_resource("schema.sql") as f:
        db.executescript(f.read().decode("utf8"))


def close_db(e=None):
    """Close the app database (it should not be called directly)."""
    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_app(app):
    """Register the db closing on app teardown."""
    app.teardown_appcontext(close_db)
