import csv
import io
from datetime import datetime

from flask import (
    Blueprint,
    request,
    redirect,
    url_for,
    flash,
    render_template,
    g,
    abort,
    make_response,
)

from .auth import login_required
from .models import Event, Project


bp = Blueprint("events", __name__, url_prefix="/events")


@bp.route("/")
def index():
    if g.current_user is None:
        events = Event.items(is_private=False)
    else:
        events = Event.items()

    return render_template(
        "events/index.html", events=events, current_date=datetime.now()
    )


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    if request.method == "POST":
        title = request.form["title"].strip()
        date = request.form["date"].strip()
        date_end = request.form["date_end"].strip()
        is_private = request.form["is_private"].strip() == "yes"
        project_name = request.form["project"].strip()
        description = request.form["description"].strip()

        if project_name:
            project = Project.find_by_name(project_name)
            if not project:
                project = Project.create(project_name, True, "", "")
            project_id = project.id
        else:
            project_id = None

        if not title:
            flash("Le titre est requis.")
        elif not date:
            flash("La date est requise.")
        elif date_end and date >= date_end:
            flash("La date de fin doit être supérieure à la date de début.")
        elif not Event.create(
            title, date, date_end, is_private, project_id, description
        ):
            flash("L’évènement n’a pas pu être ajouté.")
        else:
            flash("L’évènement a été ajouté.", "success")
            return redirect(url_for("events.index"))

    today_date = datetime.strftime(datetime.now(), "%Y-%m-%d")
    projects = Project.items()
    return render_template(
        "events/create.html", today_date=today_date, projects=projects
    )


@bp.route("/update/<event_id>", methods=("GET", "POST"))
@login_required
def update(event_id):
    event = Event.find_by_id(event_id)
    if not event:
        abort(404)

    if request.method == "POST":
        event.title = request.form["title"].strip()
        event.date = request.form["date"].strip()
        event.date_end = request.form["date_end"].strip()
        event.is_private = request.form["is_private"].strip() == "yes"
        event.description = request.form["description"].strip()

        project_name = request.form["project"].strip()
        if project_name:
            project = Project.find_by_name(project_name)
            if not project:
                project = Project.create(project_name, True, "", "")
            event.project_id = project.id
        else:
            event.project_id = None

        if not event.title:
            flash("Le titre est requis.")
        elif not event.date:
            flash("La date est requise.")
        elif event.date_end and event.date >= event.date_end:
            flash("La date de fin doit être supérieure à la date de début.")
        elif not event.save():
            flash("L’évènement n’a pas pu être modifié.")
        else:
            flash("L’évènement a été modifié.", "success")
            return redirect(url_for("events.index"))

    projects = Project.items()
    return render_template("events/update.html", event=event, projects=projects)


@bp.route("/delete/<event_id>", methods=["DELETE"])
@login_required
def delete(event_id):
    event = Event.find_by_id(event_id)
    if not event:
        flash("L’évènement n’existe pas.")
    elif not event.delete():
        flash("L’évènement n’a pas pu être supprimé.")
    else:
        flash("L’évènement a été correctement supprimé.", "success")

    return redirect(url_for("events.index"))


@bp.route("/export")
def export():
    if g.current_user is None:
        events = Event.items(is_private=False)
    else:
        events = Event.items()

    stringio = io.StringIO()
    csv_writer = csv.DictWriter(
        stringio,
        fieldnames=["titre", "date", "date de fin", "privé", "projet", "description"],
    )
    csv_writer.writeheader()
    events = sorted(events, key=lambda event: event.date)
    for event in events:
        csv_writer.writerow(
            {
                "titre": event.title,
                "date": event.date,
                "date de fin": event.date_end,
                "privé": event.is_private,
                "projet": event.project.name if event.project else "",
                "description": event.description,
            }
        )
    response = make_response(stringio.getvalue())
    response.headers["Content-Disposition"] = "attachment; filename=export.csv"
    response.headers["Content-type"] = "text/csv"
    return response
